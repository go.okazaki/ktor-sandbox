Docker Gradle
===

* [Offical](https://hub.docker.com/_/gradle)

### Install

* Script

```shell
cd <this directory>

cp -p gradle /usr/local/bin/.
```

* Wrapper(Requirement JDK)

```shell
cd <project root directory>

gradle wrapper
```
