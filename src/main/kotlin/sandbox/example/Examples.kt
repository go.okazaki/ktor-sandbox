package sandbox.example

import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.bson.types.ObjectId
import sandbox.Databases
import java.time.LocalDateTime

data class Example(
  var key: String,
  var name: String,
  var version: Long = 0,
  var id: ObjectId? = null,
  var timestamp: LocalDateTime? = null,
) {
  constructor() : this("", "")
}

object Examples {

  val collection: MongoCollection<Example> by lazy {
    Databases.getMongodb("test").getCollection("examples", Example::class.java)
  }
}

fun Application.exampleRoute() {
  routing {
    get("/examples") {
      val result = Examples.collection.find()
      call.respond(result.toList())
    }
    post("/examples") {
      val example = call.receive<Example>()
      application.log.info("example:$example")
      val result = Examples.collection.insertOne(example);
      call.respond(result.insertedId.asObjectId().value.toString())
    }
    get("/examples/{id}") {
      val id = call.parameters["id"]
      if (id != null) {
        val result = Examples.collection.find(Filters.eq("_id", ObjectId(id))).first()
        if (result != null) {
          call.respond(result)
        }
      }
    }
    delete("/examples/{id}") {
      val id = call.parameters["id"]
      if (id != null) {
        val result = Examples.collection.findOneAndDelete(Filters.eq("_id", ObjectId(id)))
        call.respond(result)
      }
    }
  }
}
