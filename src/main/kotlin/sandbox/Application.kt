package sandbox

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.netty.*
import sandbox.example.exampleRoute

fun main(args: Array<String>): Unit = EngineMain.main(args)

fun Application.module(testing: Boolean = false) {

  val develop = environment.config.propertyOrNull("ktor.environment")?.getString() != "production"
  log.info("develop:$develop")

  install(CallLogging)
  install(ContentNegotiation) {
    jackson(
      contentType = ContentType.Application.Json
    )
  }
  install(Compression) {
    gzip()
  }

  routing {
    get("/health") {
      call.respondText("OK")
    }
    exampleRoute()
  }
}
