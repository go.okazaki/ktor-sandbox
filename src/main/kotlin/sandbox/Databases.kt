package sandbox

import com.mongodb.MongoClientSettings.getDefaultCodecRegistry
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoDatabase
import com.typesafe.config.ConfigFactory
import io.ktor.config.*
import org.bson.codecs.configuration.CodecProvider
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.configuration.CodecRegistry
import org.bson.codecs.pojo.PojoCodecProvider


object Databases {

  private val client: MongoClient by lazy {
    val config = HoconApplicationConfig(ConfigFactory.load())
    val host = config.property("mongodb.host").getString()
    val port = config.property("mongodb.port").getString()
    MongoClients.create("mongodb://$host:$port")
  }
  private val pojoCodecRegistry: CodecRegistry by lazy {
    val pojoCodecProvider: CodecProvider = PojoCodecProvider.builder().automatic(true).build()
    fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider))
  }

  fun getMongodb(name: String): MongoDatabase {
    return client.getDatabase(name).withCodecRegistry(pojoCodecRegistry)
  }
}
