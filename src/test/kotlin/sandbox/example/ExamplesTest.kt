package sandbox.example

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.mongodb.client.model.Filters
import io.kotest.assertions.assertSoftly
import io.kotest.assertions.fail
import io.kotest.assertions.ktor.shouldHaveStatus
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.longs.shouldBeGreaterThan
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import sandbox.Databases
import sandbox.module

class ExamplesTest : FunSpec({

  test("mongo crud") {
    val collection = Databases.getMongodb("test").getCollection("examples", Example::class.java)
    val newExample = Example("test key", "test name")
    val insertResult = collection.insertOne(newExample) ?: fail("updateResult is null")
    assertSoftly(insertResult) {
      insertedId shouldNotBe null
    }
//    val duplicateExample = Example("test key", "test name")
//    try {
//      collection.insertOne(duplicateExample)
//      fail("")
//    } catch (e: Throwable) {
//      println(e)
//    }

    val id = insertResult.insertedId!!.asObjectId()
    val example = collection.find(Filters.eq("_id", id)).first() ?: fail("not found")
    assertSoftly(example) {
      id shouldNotBe null
    }
    val updateExample = newExample
    updateExample.name = "update name"
    val updateResult = collection.replaceOne(Filters.eq("_id", id), updateExample) ?: fail("updateResult is null")
    assertSoftly(updateResult) {
      matchedCount shouldBe 1
      modifiedCount shouldBe 1
    }
    val deleteResult = collection.deleteOne(Filters.eq("_id", id))
    assertSoftly {
      deleteResult.deletedCount shouldBeGreaterThan 0
    }
  }

  test("rest crud") {
    withTestApplication(Application::module) {
      handleRequest(HttpMethod.Get, "/examples")
        .apply {
          response shouldHaveStatus HttpStatusCode.OK
        }
      var id: String
      handleRequest(HttpMethod.Post, "/examples") {
        addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
        val example = Example("test key", "test name");
        setBody(jacksonObjectMapper().writeValueAsBytes(example))
      }
        .apply {
          response shouldHaveStatus HttpStatusCode.OK
          id = response.content.toString()
          id shouldNotBe null
        }
      handleRequest(HttpMethod.Get, "/examples/$id")
        .apply {
          response shouldHaveStatus HttpStatusCode.OK
        }
      handleRequest(HttpMethod.Delete, "/examples/$id")
        .apply {
          response shouldHaveStatus HttpStatusCode.OK
        }
    }
  }
})
