package sandbox

import io.kotest.assertions.ktor.shouldHaveStatus
import io.kotest.core.spec.style.FunSpec
import io.ktor.http.*
import io.ktor.server.testing.*

class ApplicationTest : FunSpec({

  test("health") {
    withTestApplication({ module(testing = true) }) {
      handleRequest(HttpMethod.Get, "/health").apply {
        response shouldHaveStatus HttpStatusCode.OK
      }
    }
  }
})
