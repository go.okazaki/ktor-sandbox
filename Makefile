DOCKER_REGISTRY ?= localhost
VERSION_TAG ?= latest
IMAGE_TAG = $(DOCKER_REGISTRY)/ktor-sandbox:$(VERSION_TAG)

all:
	./gradlew assemble

test:
	./gradlew check

install:
	docker build --build-arg JAR_FILE=build/libs/ktor-sandbox.jar -t $(IMAGE_TAG) .

clean:
	./gradlew clean
