// https://github.com/ktorio/ktor/releases
var ktorVersion by extra("1.6.7")

plugins {
  // https://kotlinlang.org/docs/releases.html#release-details
  kotlin("jvm") version "1.6.10"
  kotlin("plugin.serialization") version "1.6.10"
  id("application")
  // https://github.com/Kotlin/kotlinx-kover/releases
  id("org.jetbrains.kotlinx.kover") version "0.5.0-RC"
  // https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow
  id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "sandbox"
version = "0.0.1-SNAPSHOT"
//java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
  mavenCentral()
}

application {
  mainClass.set("sandbox.ApplicationKt")
}

dependencies {
  implementation("io.ktor:ktor-server-netty:$ktorVersion")
//  implementation("io.ktor:ktor-serialization:$ktorVersion")
  implementation("io.ktor:ktor-jackson:$ktorVersion")
  implementation("ch.qos.logback:logback-classic:1.2.10")
  implementation("org.mongodb:mongodb-driver-sync:4.4.0")

  testImplementation("org.jetbrains.kotlin:kotlin-test")
  testImplementation("io.ktor:ktor-server-test-host:$ktorVersion")
  testImplementation("io.kotest:kotest-runner-junit5-jvm:5.0.3")
  testImplementation("io.kotest.extensions:kotest-assertions-ktor:1.0.3")
}

tasks {
  shadowJar {
    archiveFileName.set("${rootProject.name}.jar")
  }
  withType<Test> {
    useJUnitPlatform()
  }
}
