FROM eclipse-temurin:17-alpine

RUN addgroup -S java && adduser -S java -G java
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
USER java
ENTRYPOINT java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar
